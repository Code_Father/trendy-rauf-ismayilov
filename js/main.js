/*Slider 1*/

$('.works-slider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: $(".arrow-prev"),
    nextArrow: $(".arrow-next")
});

//Slider 2
$(document).ready(function(){
    $('.carousel-testimonials').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 2000,
        mobileFirst: true,
    });
});


